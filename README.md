# Cuetils

cuesheet and playlist parser with related cue utilities

For now, the main use of this program is to split a single media file
into multiple chunks using *ffmpeg* and tag the result chunks on the go
according to a tracks list provided by the user.


## Requirements

 * Python 3.7
 * `ffmpeg` and `ffprobe` executable binaries in your system PATH


## Usage

Run the `cueproc` program from a console with the appropriate arguments.


### Arguments

The following arguments are supported.

*name of a tracks list*
:   the name of a text file to parse. The file contains information 
    about the tracks held in a media file. The media file is referred 
    from the text file using the "FILE" tag or cue. The list of tracks 
    must conform to one of the syntaxes supported by `cuetils`.

*output syntax*
:   the syntax used to print the output.


### Examples

    cueproc samples/1.cue

Will parse the contents of the file `1.cue` available in the `samples` 
folder and print a core data structure.

    cueproc samples/5.info ffmpeg

Will chunk the `hjqk2vlkAno.oga` media file referred from 
`samples/5.info` into multiple tagged tracks named after their 
tracknumber (`hjqk2vlkAno-01.oga`, `hjqk2vlkAno-02.oga`, etc.). 
Programs other than ffmpeg may be used for this chunking task in the 
future.

    cueproc samples/5.info cue

Will parse the `samples/5.info` tracks list and print it as a cuesheet.


### Supported syntaxes

`txt`
:   human-readable text format, often used in Youtube video comments. 
    See the `*.info` files in the `samples/` directory. The "text" 
    syntax is either index-based (the first track matches the "0" 
    offset) or duration-based (every track has a matching length)

`cue`
:   old and popular machine-readable command list format used as an 
    input by CD-ROM burning applications. See the `*.cue` files in the 
    `samples/`directory. The cuesheet syntax is index-based.

`ffmpeg`
:   (output only) a set of command-line arguments for chunking and 
    tagging a media file named in the tracks list. Each chunk matches a 
    time cue in the input tracks list. If the tracks list does not 
    refer a media file (with the FILE tag or cue) or if the refered 
    media file is not found or if no `ffmpeg` binary is available in 
    your PATH, then the batch list of chunking/tagging commands is 
    printed out with no further processing.


### Notes

A tracks list syntax is either length-based (every track is provided 
with a matching duration) or index-based (every track is provided with 
a matching offset from 0). Cuetils should be able to parse and build 
tracks lists written in any of the supported syntaxes.

Also the time units may either be seconds or CDDA-frames. Cuetils 
provides classes for both and should be able to guess the right time 
format according to the matching syntax specification.


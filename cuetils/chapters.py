#!/usr/bin/env python3
"""simple chapters"""

import cuetils.timing
from typing import Self, Optional

class Track(object):
    
    def __init__(self, start:str, name:str) -> None:
        """"""
        self.start = cuetils.timing.Time.parse(start)
        self.metadata = Metadata({"name": name})
        self.index:int = 0
    
    def __str__(self) -> str:
        """"""
        data = []
        n = "%02d" % self.index
        for k,v in {
            f"CHAPTER{n}":self.start.format(),
            f"CHAPTER{n}NAME":self.metadata.name
        }.items():
            data.append("=".join((k,v)))
        return "\n".join(data)
    
    def __repr__(self) -> str:
        """"""
        #label = f"<{type(self).__name__} {self.index}> "
        return ":".join((repr(i) for i in
            (self.start.format(),self.metadata.name)))
    
    @classmethod
    def parse(cls, s:str) -> Optional[Self]:
        """"""
        start, name = None, None
        data = s.strip().split("\n")
        if len(data) == 2:
            start, name = (i.strip() for i in data)
            name = data[1][1+data[1].find("="):].strip()
            index, start = (i.strip() for i in data[0].split("="))
            track = Track(start,name)
            track.index = int(index.lower().lstrip("chapter"))
            return track


class Tracks(list):
    
    @classmethod
    def parse(cls, s:str) -> Self:
        """"""
        tracks = []
        entries = tuple(i for i in s.split("\n") if
            i.strip().lower().startswith("chapter") and "=" in i)
        for entry in (entries[i:i+2] for i in range(0, len(entries), 2)):
            track = Track.parse("\n".join(entry))
            if track:
                tracks.append(track)
        return Tracks(tracks)
        
    @classmethod
    def from_dict(cls, d:dict) -> Self:
        """"""
        tracks = []
        for index,(start,name) in enumerate(d.items()):
            track = Track(start,name)
            if track:
                track.index=index
                tracks.append(track)
        return Tracks(tracks)
    
    def __repr__(self) -> str:
        """"""
        s=",".join((repr(track) for track in self))
        return "{%s}" % s
    
    def __str__(self) -> str:
        """"""
        return "\n".join(str(track) for track in self)


class Metadata(object):
    
    def __init__(self, d:dict={}) -> None:
        """metadata as name:value dict"""
        _supported = "title name".split()
        for k,v in d.items():
            if k.lower() in _supported:
                self.__dict__[k.lower()] = v
        if all(("title" in d,"name" not in d)):
            d.update({"name": d.pop("title")})
        self.__dict__.update(d)


if __name__ == "__main__":
    tracks = Tracks.parse("""
        CHAPTER000     = 00:00:00
        CHAPTER00NAME  = intro
        CHAPTER01      = 00:01:30
        CHAPTER001NAME = chapter one
        CHAPTER02      = 01:30:22
        CHAPTER02NAME  = end credits
        CHPTR02       -> 01:30:22
        CHPTR02NAME    = broken chapter (will be discarded)
    """)
    print("#repr\n",repr(tracks),sep="")
    print("\n#parsed from str\n",str(tracks), sep="")
    print("\n#from dict\n",Tracks.from_dict({"00:01:01":"untitled"}), sep="")


#!/usr/bin/env python3
# coding: utf-8
"""convert a list of tracks to a scarse XMCD file

example:
    First Class
    Time (You and I)
    Connaissais de Face
    Father Bird, Mother Bird
    If There Is No Question
    Pelota
    One To Remember
    Dearest Alfred
    So We Won’t Forget
    Shida

becomes:
    # xmcd CD database file
    # Track frame offsets: {}
    # Disc length: {} seconds
    # Submitted via: {}
    CATEGORY=
    DISCID=
    DTITLE=
    DYEAR=
    DGENRE=
    TTITLE0=First Class
    TTITLE1=Time (You and I)
    TTITLE2=Connaissais de Face
    TTITLE3=Father Bird, Mother Bird
    TTITLE4=If There Is No Question
    TTITLE5=Pelota
    TTITLE6=One To Remember
    TTITLE7=Dearest Alfred
    TTITLE8=So We Won’t Forget
    TTITLE9=Shida
    EXTD=
    PLAYORDER=
"""
import sys

XMCD_PROLOG=("xmcd CD database file","Track frame offsets: {}",
             "Disc length: {} seconds","Submitted via: cuetils")
XMCD_KEYWORDS_FIRST=("CATEGORY","DISCID","DTITLE","DYEAR","DGENRE")
XMCD_KEYWORDS_LAST=("EXTD","PLAYORDER")

def convert(fn):
    """"""
    def _parse(lines):
        """"""
        for line in lines:
            line = line.strip() or None
            if line:
                yield line
    
    def _titles(tracks):
        """"""
        for pos, track in enumerate(tracks):
            yield "=".join(('TTITLE{}'.format(pos),track))
    
    tracks=tuple(_parse(fn))
    prolog="\n".join(("# {}".format(i) for i in XMCD_PROLOG))
    titles="\n".join((t for t in _titles(tracks)))
    kw_first="\n".join(("{}=".format(k) for k in XMCD_KEYWORDS_FIRST))
    kw_last="\n".join(("{}=".format(k) for k in XMCD_KEYWORDS_LAST))
    return "\n".join((i for i in (prolog,kw_first,titles,kw_last)))

if __name__ == "__main__" and len(sys.argv) > 1:
    with open(sys.argv[1]) as fn:
        print(convert(fn))
        """
        with open(fn.name+".xmcd","w") as out:
            out.write(convert(fn))
        """


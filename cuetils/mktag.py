import configparser
from cuetils import ffmpeg
import subprocess as SP
import xml.etree.ElementTree as ET

#TODO: manage tags as config from (ini) instead of {**tags}
#TODO: warn or abort when loading tags from non-matroska

class MkvProps(object):
    """Matroska simple tags editor"""

    def __init__(self, mediafile=None, cfgfile=None):
        """import tags from a media file and a configuration (ini) file"""
        self.mediafile = mediafile
        self.configfile = cfgfile
        self.config = configparser.ConfigParser(interpolation=None)
        self.tags = {}
        if self.configfile and self.configfile.exists():
            self.parse_ini()
        if self.mediafile and self.mediafile.exists():
            self.ffprobe()
            self.mkvextract()

    def parse_ini(self):
        """import tags from a configuration (ini) file"""
        self.config.read(self.configfile)
        if self.mediafile.name in self.config:
            self.tags.update(self.config[self.mediafile.name])

    def mkvextract(self):
        """call mkvextract to get XML simpletags"""

        def _read(xml):
            """translate Matroska simpletags tree"""
            for tag in xml.findall(".//Simple"):
                if tag is not None:
                    get = lambda key: str(tag.find(key).text)
                    name, value = get("Name"), get("String")
                    self.tags.update({name.lower(): value})

        xml, xmlfile = None, self.mediafile.with_suffix(".xml")
        if xmlfile.exists():
            try:
                xml = ET.parse(xmlfile)
            except ET.ParseError as e:
                print(e)
        else:
            with open(self.mediafile):
                cmd = ('mkvextract','tags', self.mediafile)
                out = SP.run(cmd,stdout=SP.PIPE).stdout or None
                err = out is None or out.startswith(b"Error:")
                if not err:
                    try:
                        xml = ET.fromstring(out)
                    except ET.ParseError as e:
                        with xmlfile.open("wb") as f: f.write(out)
        if xml: _read(xml)

    def ffprobe(self):
        """get title from ffprobe"""
        title = self.tags.get("title"
                             ) or ffmpeg.FFProbe(self.mediafile)("title")
        self.tags.update({"title":title})

    def _tree(self, targets=None):
        """set and add a simpletag for every item in a dict

        Matroska Tag Specifications
        :   <http://www.matroska.org/technical/specs/tagging/index.html>

        """
        tags = ET.Element("Tags")
        tag = ET.SubElement(tags, "Tag")
        _targets = ET.SubElement(tag,"Targets")
        if targets is not None:
            ttype = ET.Element("TargetType")
            ttype_value = ET.Element("TargetTypeValue")
            ttype_value.text, ttype.text = targets # "50", "MOVIE"
            _targets.extend((ttype_value, ttype))
        for k,v in self.tags.items():
            name, value = ET.Element("Name"), ET.Element("String")
            name.text, value.text = k.upper(), v
            _current = ET.Element("Simple")
            _current.extend((name,value))
            tag.append(_current)
        return ET.ElementTree(tags)

    def __str__(self):
        """return an XML representation of the tree"""
        root = self._tree().getroot()
        return ET.tostring(root,encoding="unicode")

    def query(self, tag=None):
        """query a tag for a formatted name=value string

        if no specific tag is queried, then all available are shown

        """

        def format(tag):
            """format a tag for display"""

            def quote(x):
                """add double quotes to string when needed"""
                _str = str(x)
                return f"\"{_str}\"" if " " in _str else x

            return "%s=%s" % (tag, self.tags.get(tag))

        return (
            format(tag) if tag
            else "\n".join([format(i) for i in self.tags])
        )

    def merge(self, edits={}):
        """merge and cleanup"""
        self.tags.update(edits)
        unwanted = ("""
            handler_name major_brand minor_version vendor_id source_id
            compatible_brands _statistics_writing_date_utc
            _statistics_writing_app
            """).split()
        unwanted.extend([i for i in self.tags
            if self.tags[i] in (None,"None","")])
        for i in unwanted:
            if i in self.tags:
                self.tags.pop(i)

    def _export(self, tagsfile):
        """export properties to an XML file"""
        tree = self._tree()
        tree.write(str(tagsfile), encoding="unicode", xml_declaration=True)

    def write(self):
        """write properties into a media file"""

        def _mkvpropedit(tagsfile):
            """set an mkvpropedit command to write tags

            mkvpropedit documentation
            :   <https://www.bunkus.org/videotools/mkvtoolnix/doc/mkvpropedit.html>

            """
            cmd, args = ['mkvpropedit', str(self.mediafile)],[]
            _title = self.tags.get("title")
            if _title:
                args.extend(
                    #("--edit","info","--set",f"title={_title}") ##set back if the follwing doesn't work
                    ("--edit","info","--delete","title")
                )
            _image = self.mediafile.with_suffix(".jpg")
            if _image.exists():
                args.extend((
                    "--attachment-name", "cover.jpg",
                    "--attachment-description", "cover art",
                    "--attachment-mime-type", "image/jpeg",
                    "--attachment-file", str(_image)
                ))
                """
                replacing:
                    "--replace-attachment", f"name:cover.jpg:{_image.name}"
                """
            if len(self.tags):
                args.extend(("--tags",f"all:{tagsfile}"))
            if len(args):
                cmd.extend(args)
                return cmd

        with self.mediafile.open():
            """write temp tags file"""
            tagsfile = self.mediafile.with_suffix(".tmp.tags.xml")
            cmd = _mkvpropedit(tagsfile)
            if cmd:
                with tagsfile.open('w'):
                    self._export(tagsfile)
                    try:
                        exe = SP.run(cmd,check=True,stderr=SP.PIPE)
                        if exe.stderr:
                            print(str(cmd))
                    except SP.CalledProcessError as error:
                        raise error
                tagsfile.unlink()



"""simple Cue sheet parser"""
import pathlib
import sys
import cuetils.core
import cuetils.timing

CUEMETA = ("TITLE","PERFORMER","SONGWRITER")
DEBUG = False

class Sheet(cuetils.core.Sheet):
    """a cue sheet as list of Tracks with additional metadata"""

    @classmethod
    def parse(cls, cues):
        """parse the sheet content cues
        
        parse metadata and tracks information into separate lists
        then gather them together into a sheet.
        
        Note : One Tracks instance for every FILE cue.
        
        """
        _files = ([i for i,cue in enumerate(cues) if cue[0] == "FILE"])
        _sheet, _head, _body = [], cues[:_files[0]], cues[_files[0]:]
        metadata = Metadata.parse(_head)
        
        def sections(items, by):
            """generate groups of items"""
            groups = [i for i in range(len(items)) if items[i][0] == by]
            for cur,next in zip(groups, groups[1:]+[None]):
                _group = items[cur:next]
                yield _group
        
        for files in sections(_body,"FILE"):
            tracks, mediafile = [], pathlib.Path(files[0][1])
            for track in sections(files,"TRACK"):
                tracks.append(Track.parse(track))
            _sheet.append(Tracks(tracks, mediafile))
        return Sheet(_sheet, metadata)
    
    @classmethod
    def open(cls, fpath):
        """import cues from a cuesheet file"""
        cues = []
        with fpath.open() as f:
            for line in f.readlines():
                cue = Cue(line)
                if cue:
                    cues.append(cue)
        if len(cues) > 0:
            sheet = cls.parse(cues)
            sheet.basedir = fpath.parent
            return sheet
    
    @classmethod
    def wrap(cls, core):
        """wrap an existing sheet"""
        _sheet = []
        for tracks in core:
            _tracks = []
            for track in tracks:
                _tracks.append(Track(track.time.to_frames(), track.metadata))
            _sheet.append(Tracks(_tracks, tracks.mediafile))
        return Sheet(_sheet, Metadata(core.metadata), core.basedir)
    

class Tracks(cuetils.core.Tracks):
    """a group of tracks that match a single FILE"""
     
    def __str__(self):
        """print a formatted output of a list of tracks"""
        _str = [f"FILE {repr(str(self.mediafile))} WAVE"]
        _str.extend(f"{track}" for track in self)
        return "\n".join(_str)
    

class Track(cuetils.core.Track):
    """a core Track with additional cues parsing method"""
     
    def __str__(self):
        """print the formatted output of a track"""
        _str=[]
        report = [
            f"TRACK {self.number:02d} {self.datatype}",
            f"INDEX 01 {self.start.format()}"]
        title = self.metadata.get('title') or ""
        report.append(f"TITLE {repr(title)}")
        if self.duration and DEBUG:
            report.append(f"REM __DURATION__ {repr(self.duration.format())}")
        for i in report:
            indent = 2 if i.startswith("TRACK") else 4
            _str.append(f"{' '*indent}{i}")
        return "\n".join(_str)
    
    @classmethod
    def parse(cls, cues):
        """create normative cue track from a set of commands"""
        
        def datatype(cue):
            """track datatype"""
            if len(cue) > 1:
                return cue[2].strip() or "AUDIO"
        
        def number(cue):
            """track number"""
            if len(cue) > 0:
                return int(cue[1])
        
        def start_index(cues):
            """parse the start index of a track"""
            indices = ([i for i in cues if i[0] == "INDEX"])
            indices.sort(key=lambda idx: idx[1])
            start = indices[0][2]
            return cuetils.timing.Time.parse(start)
        
        cue = ([i for i in cues if i[0] == "TRACK"])[0]
        return Track(start_index(cues), Metadata.parse(cues),
                     number(cue), datatype(cue))
    

class Metadata(cuetils.core.Metadata):
    """metadata to be used in both Sheet and Track"""
     
    def __str__(self):
        """print a human-readable list of metadata"""
        cues, _rem, _excluded = [], [], ("FILE")
        for k, v in self.items():
            k = k.upper()
            if k not in (_excluded):
                meta = f"{k} {repr(v)}"
                if k in CUEMETA:
                    cues.append(meta)
                else:
                    _rem.append(f"REM {meta}")
        cues.extend(_rem)
        return "\n".join((cue for cue in cues))
    
    def normalize(self):
        """enforce the CDTEXT metadata convention"""
        if self.get("artist") and not self.get("performer"):
            self["performer"] = self.pop("artist")
        if self.get("album") and not self.get("title"):
            self["title"] = self.pop("album")
    
    @classmethod
    def parse(cls, cues):
        """parse a list to create a normative metadata object"""
        
        def _get(meta):
            """get joined values for cue-metadata with same names"""
            r = ", ".join((i[1] for i in cues if i[0] == meta))
            return r.strip() or None
        
        metadata = {}
        for i in CUEMETA:
            meta = _get(i)
            if meta:
                metadata.update({i.lower():meta})
        for i in cues:
            if i[0]=="REM":
                metadata.update({i[1].lower():i[2]})
        return Metadata(metadata)

class Cue(tuple):
    """cue entry parsed as tuple"""
    
    def __new__(cls, line):
        """renew the immutable tuple class instance"""
        parsed = cls.parse(line)
        if parsed:
            return tuple.__new__(cls, parsed)
    
    @classmethod
    def parse(cls, line):
        """parse a cue command from a string
        
        Let a supported cue command be parsed according to its type.
        Unsupported commands are stripped out.
        
        """
        line = line.strip() or None
        if (line and not line.startswith('#')):
            cues = line.split()
            if len(cues) > 1:
                jstrip = lambda s: " ".join(s).strip("\"\'`")
                cmd = cues[0].upper()
                if cmd == "REM" and len(cues) > 2:
                    return cmd, cues[1], jstrip(cues[2:])
                elif cmd == "FILE":
                    return cmd, jstrip(cues[1:-1]), cues[-1]
                elif cmd in CUEMETA:
                    return cmd, jstrip(cues[1:])
                elif cmd in ("TRACK","INDEX"):
                    return cues
    

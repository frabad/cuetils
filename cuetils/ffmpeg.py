#!/usr/bin/env python3
""""""

import subprocess as sp
import json
import sys
import pathlib
from cuetils import timing

"""
TODO:add support for writing metadata with
    https://ikyle.me/blog/2020/add-mp4-chapters-ffmpeg
"""

class Struct:
    """primitive structure that provides object-notation query to dict items"""

    def __init__(self, d):
        """set all keys to lower case, all subdict to Struct"""
        for k, v in d.items():
            if type(v) is dict:
                self.__dict__[k.lower()] = Struct(v)
            elif type(v) in (list, tuple):
                self.__dict__[k.lower()] = tuple(Struct(item) if (
                    type(item) is dict) else item for item in v)
            else:
                self.__dict__[k.lower()] = v

    def __contains__(self, key: str) -> bool:
        """allow the 'in' keyword"""
        return key in self.__dict__

    def __repr__(self) -> str:
        """show structure as dict"""
        return repr(self.__dict__)

    def get(self, key: str) -> object:
        """get the object that matches the provided key"""
        return self.__dict__.get(key)

    def _traverse(dic, path=None):
        """TODO"""
        if not path:
            path=[]
        if isinstance(dic,dict):
            for x in dic.keys():
                local_path = path[:]
                local_path.append(x)
                for b in traverse(dic[x], local_path):
                    yield b
        else:
            yield path,dic
    

class FFProbe(Struct):
    """a collection of metadata with query facilities"""
    
    def __init__(self, mediafile: pathlib.Path):
        """feed and organize probe data"""
        self.mediafile = mediafile
        super().__init__(self.load())
        self.video, self.audio, self.subtitle, self.tags = [],[],[], None
        for stream in self.streams:
            match stream.get("codec_type"):
                case "video": self.video.append(stream)
                case "audio": self.audio.append(stream)
                case "subtitle": self.subtitle.append(stream)
        if "tags" in self.format:
            self.tags = self.format.tags
        elif "tags" in self.streams[0]: # FIXME stream selection logic
            self.tags = self.streams[0].tags
        self.probed = {}
    
    def __call__(self, key:str):
        """call a formatted result from a key"""
        key, result = key.lower(), None
        if not any(("codec" in key, "bit_rate" in key,
            key in ("duration","fps","depth","chapters"))
            ): # first search filter
            result = self.format.get(key) or next(
                (d.get(key) for d in self.streams if d.get(key)),None)
        if key in ("title","artist","album","date","genre","author",
            "performer","description","comment","synopsis","purl"
            ): # check for metadata tag
            result = ((self.tags.get(key)
                or self.tags.get(f"movie/{key}"))
                if self.tags else None)
        if "bit_rate" in key: # search through and convert to Kbps
            result = self.format.get(key)
            if key.startswith("video_") and len(self.video):
                key = key.lstrip("video_")
                result = self.video[0].get(key)
                if result is None and "tags" in self.video[0]:
                    result = self.video[0].tags.get("BPS-eng")
            elif key.startswith("audio_") and self.audio:
                key = key.lstrip("audio_")
                result = self.audio[0].get(key)
            result = round(int(result) / 1000) if result else None
        if "codec" in key: # video codec or audio codec or both"""
            result = []
            add = lambda x: result.extend(i.get("codec_name") for i in x)
            if "video" in key: add(self.video)
            elif "audio" in key: add(self.audio)
            elif "sub" in key: add(self.subtitle)
            else: add(self.streams)
            result = result[0] if len(result) == 1 else result
        match key:
            case "duration": # make the duration a Seconds object
                result = self.format.get(key)
                result = timing.Seconds(result).format() if result else None
            case "fps": # fix the frame rate (FPS) in the video streams
                for i in ("r_frame_rate", "avg_frame_rate"):
                    result = next((d.get(i) for d in self.video if i in d),
                    None)
                if result and "/" in result:
                    x,y = result.split("/")
                    result = (x if int(y) in (0,1)
                        else "%.3f" % float(int(x) / int(y))).split(".000")[0]
            case "depth": # height as depth
                if self.video:
                    result = self.video[0].get("height")
                    result = str(result)+"p" if result else result
            case "chapters":
                if len(self.chapters):
                    result = [f"{c.get('start_time')}: "
                        f"{c.get('tags').get('title')}" for c in self.chapters]
        self.probed.update({key:result})
        return result

    def load(self) -> dict:
        """load json data from system ffprobe result

        output is similar to the following:
            {
                "streams": [
                    {
                        "index","codec_name","codec_long_name",
                        "codec_type","codec_time_base","codec_tag_string",
                        "codec_tag","width","height","has_b_frames",
                        "pix_fmt","level","is_avc","nal_length_size",
                        "r_frame_rate","avg_frame_rate","time_base",
                        "start_time","duration","bit_rate","nb_frames",
                        "tags"
                    }
                ],
                "format": {
                    filename,nb_streams,nb_programs,format_name,
                    format_long_name,start_time,duration,size,bit_rate,
                    probe_score,tags
                },
                "chapters": [
                    {
                        "id","time_base","start","start_time","end",
                        "end_time","tags": {"title"}
                    },
                ]
            }
        """
        cmd = ("ffprobe -loglevel quiet -print_format json "
            "-show_format -show_streams -show_chapters").split()
        cmd.append(self.mediafile)
        proc = sp.run(cmd, capture_output=True, text=True)
        return json.loads(proc.stdout or "{}")

    def report(self):
        """display formatted dict of probed keys"""
        _report = [f"[{self.mediafile}]"]
        longest = max(len(i) for i in self.probed if self.probed.get(i))
        for k,v in self.probed.items():
            tab = (longest-len(k))*' '
            if v is not None:
                _report.append(f"{k}{tab} : {v}")
        return "\n".join(_report)
        
if __name__ == "__main__" and len(sys.argv)>1:    
    mediafile = pathlib.Path(sys.argv[1])
    if not(mediafile.exists()):
        raise SystemExit(f"'{mediafile}' does not exist.")
    probe = FFProbe(mediafile)
    wanted = sys.argv[2:]
    if len(wanted) < 1:
        wanted = ("title","artist","album","duration",
        "display_aspect_ratio","depth","fps","video_bit_rate",
        "audio_bit_rate","bit_rate","codec","chapters")
    for i in wanted: probe(i)
    print(probe.report())


#/usr/bin/env python3
"""
description:
    
    Calculates a disc id as a 32-bit integer, which we represent
    using 8 hex digits XXYYYYZZ. 

      - XX is the checksum. The checksum is computed as follows:
        for each starting frame[i], we convert it to seconds by
        dividing by the frame rate 75; then we sum up the decimal
        digits. E.g., if frame[i] = 7500600, this corresponds to
        100008 seconds whose digit sum is 1 + 8 = 9.
        XX is the total sum of all of these digit sums mod 255.
      - YYYY is the length of the album tracks in seconds. It is 
        computed as (frames[N] - frames[0]) / 75 and output in hex.
      - ZZ is the number of tracks N expressed in hex.
    
SeeAlso:
    
    https://pythonhosted.org/python-libdiscid/api.html
    http://www.cs.princeton.edu/introcs/51data/CDDB.java.html
"""

class Disc(object):
    
    FPS = 75 # frames per second

    def __init__(self, *track_lengths):
        """"""
        self.pregap = Disc.FPS * 2
        self.first_track = 1
        self.track_lengths = track_lengths
        self.last_track = len(self.track_lengths)
        self.track_offsets = self.track_offsets()
        self._sectors = self.track_offsets[-1]-self.track_offsets[0]
        self.length = self._sectors
        self.seconds = int(self.length / Disc.FPS)
        self.checksum = self.checksum()
        self.freedb_id = self.freedb_id()
        self.toc = str(self)
    
    def track_offsets(self):
        """tuple of all track offsets (in sectors)
        
        The first element corresponds to the offset of the track
        denoted by first_track and so on.
        """
        offsets = [self.pregap]
        for i in range(len(self.track_lengths)):
            offsets.append(self.pregap+self.track_lengths[i])
        return tuple(offsets)
    
    def checksum(self):
        """summed-up decimal digits of the start offsets in seconds"""
        
        def sum_of_digits(n):
            """a sum of decimal digits in n"""
            _sum = 0
            while n > 0:
                _sum = _sum + (int(n) % 10)
                n = n / 10
            return int(_sum)
        
        _sum = 0
        for i in range(self.last_track):
            _sum += sum_of_digits(self.track_offsets[i] / Disc.FPS)
        return int(_sum % 255)
    
    def freedb_id(self):
        """the FreeDB Disc ID (without category)
        
        returns:
            a 32-bit integer represented as a 8-digits hex value
        """
        disc_id = (
            (self.checksum << 24) | (self.seconds << 8) | self.last_track)
        return hex(disc_id).lstrip("0x")
    
    def __str__(self):
        """string representing the CD’s Table of Contents (TOC)"""
        entries = []
        gap = Disc.FPS*2
        for i in range(self.last_track):
            offset = self.track_offsets[i]
            entries.append(
                f"\ttrack {i+1:2d}:"
                f"\t\t{offset}"
                f"\t({gap} + {offset-gap})")
        entries.append(f"\t  length:\t\t{self.length}")
        return "\n".join(entries)


if __name__ == "__main__":
    """"""
    lengths=(14522, 27217, 44880, 60395, 76557, 103495, 116280,
        137580, 156737, 171427, 185642, 208350)
    explanation="""
    Pearl Jam's album Vs. has N = 12 tracks. The first track
    starts at frames[0] =  150, the second at frames[1] = 14672,
    the twelfth at frames[11] = 185792, and the disc ends at
    frames[N] = 208500. Its disc id is 970ADA0C.
    """
    disc=Disc(*lengths)
    print(
        f"track_lengths:\n\t{disc.track_lengths}",
        f"\ntoc:\n{disc.toc}",
        f"\nfreedb_id:\n\t{disc.freedb_id}",
        f"\nexplanation:{explanation}")


"""simple text trackslist parser"""
import pathlib
import re
import sys
import cuetils.core
import cuetils.timing

DEBUG = False

def indent(s):
    """indent a string"""
    return " "*(40-len(s))


class Sheet(cuetils.core.Sheet):
    """a global sheet as list of Tracks with additional metadata"""
    
    @classmethod
    def parse(cls, cues):
        """parse the sheet content cues
        
        parse metadata and tracks information into separate lists
        then gather them together into a sheet
        
        """
        
        def parse_main_meta(data):
            """parse main metadata in compact form"""
            _wanted = ("artist","album","date")
            _pattern = re.compile(
                "^(?P<artist>[^/]+)"
                r"\s+/\s+(?P<album>[^(]+)"
                r"\s+(\((?P<date>\d{4}(\-\d{2})?(\-\d{2})?)\))?")
            _match = _pattern.search(data)
            if _match:
                return {key: _match.group(key).strip() for key in _wanted}
        
        _sheet, tracks, metadata, mediafile = [], [], {}, None
        compact_main_meta = parse_main_meta(cues[0])
        if compact_main_meta:
            metadata.update(compact_main_meta)
            cues = cues[1:]
        for cue in cues:
            if cue.startswith("#"):
                cue = cue.strip("#").split()
                if len(cue) > 1:
                    key, value = cue[0].lower(), " ".join(cue[1:])
                    if key == "file": mediafile = pathlib.Path(value)
                    metadata.update({key:value})
            else:
                track = Track.parse(cue)
                if track: tracks.append(track)
        _sheet.append(Tracks(tracks, mediafile))
        return Sheet(_sheet, Metadata(metadata))
    
    @classmethod
    def open(cls, fpath):
        """import cues from a text file"""
        cues = []
        with fpath.open() as f:
            for line in f.readlines():
                cue = line.strip() or None
                if cue:
                    cues.append(cue)
        if len(cues) > 0:
            sheet = cls.parse(cues)
            sheet.basedir = fpath.parent
            return sheet
    
    @classmethod
    def wrap(cls, core):
        """wrap an existing sheet"""
        _sheet = []
        for tracks in core:
            _tracks = []
            for track in tracks:
                _tracks.append(Track(track.time.to_seconds(), track.metadata))
            _sheet.append(Tracks(_tracks, tracks.mediafile))
        return Sheet(_sheet, Metadata(core.metadata), core.basedir)
    

class Tracks(cuetils.core.Tracks):
    """a group of tracks"""
    
    def __str__(self):
        """print a formatted output of every track in a list"""
        return "\n".join([str(i) for i in self])
    

class Track(cuetils.core.Track):
    """a track"""
    
    def __str__(self):
        """print human-readable version of a track"""
        cues = []
        cue = f"{self.number:02d}. "
        artist = self.metadata.get("artist")
        title = self.metadata.get("title")
        if artist: cue = cue + artist + " / "
        if title: cue = cue + title
        cues.extend((cue,indent(cue),f"{self.start.format()}"))
        if self.duration and DEBUG:
            cues.append(f"# duration {self.duration.format()}")
        return " ".join(cues)
    
    @classmethod
    def parse(cls, _str):
        """parse data from a single-line string"""
        
        def get_tracknumber(data):
            """get ignorable leading tracknumber from string data"""
            number_pattern = re.compile(r"^\d{1,2}?\s?[\.\-:_]?\s")
            result = number_pattern.match(data) or None
            if result:
                return result.group()
        
        _str = _str.split("#")
        data = " ".join(_str[0].split())
        userdata = "#".join(_str[1:]) if len(_str) > 1 else None
        tracknumber = get_tracknumber(data)
        if tracknumber:
            data = data.lstrip(tracknumber)
        time = cuetils.timing.Time.parse(data)
        if time is not None: # 0 is a valid time value
            data = data.strip(time.string)
            if userdata:
                data = "#".join([data,userdata])
            metadata = Metadata.parse(data)
            return Track(time,metadata)
    

class Metadata(cuetils.core.Metadata):
    """metadata specific to text sheet"""
    
    def __str__(self):
        """print a human-readable output of metadata"""
        nodisplay = [None]
        _str = []
        for m, d in self.items():
            cues = []
            if m not in nodisplay:
                cue = f"#   {m}"
                cues.extend((cue, indent(cue), f"{d}"))
                _str.append(" ".join(cues))
        return "\n".join(_str)
    
    @classmethod
    def parse(cls, _str):
        """parse metadata from string"""
        
        def _get(cues):
            """make every two-or-more-words string a metadata"""
            metadata = {}
            for cue in cues:
                cue = cue.split()
                if len(cue) > 1:
                    metadata.update({cue[0]: " ".join(cue[1:])})
            return metadata
        
        metadata = {}
        cues = _str.split("#")
        data = cues[0].strip()
        various_artist, separator = False, ""
        for s in ("-","/","\\"):
            if s + " " in data:
                datas = data.split(s)
                if len(datas) == 2:
                    data = datas
                    various_artist = True
                    separator = s
        if various_artist:
            if separator == "\\":
                metadata.update({"artist":data[1],"title":data[0]})
            else:
                metadata.update({"artist":data[0],"title":data[1]})
        else:
            metadata.update({"title":data})
        if len(cues) > 1:
            metadata.update(_get(cues[1:])) # user data
        return Metadata(metadata)



"""
core trackslist model

"""
import pathlib
import subprocess
import cuetils.timing

class Sheet(list):
    """a global sheet as list of Tracks with additional metadata"""
    
    def __init__(self, tracks, metadata={},
                 basedir=pathlib.Path.cwd()):
        """sheet constructor"""
        self.extend(tracks)
        self.metadata = metadata
        self.basedir = basedir
        self.sync()
    
    def __repr__(self):
        """default contents representation"""
        _str = [self.metadata]
        for tracks in self:
            _str.append(tracks)
        return "\n".join((str(i) for i in _str))

    def ffmpeg(self):
        """call the chunk method on every tracks list in the sheet"""
        for tracks in self:
            tracks.ffmpeg()
    
    def sync(self):
        """spread metadata down over every track in the sheet"""
        self.metadata.normalize()
        for tracks in self:
            for track in tracks:
                for m in self.metadata.keys():
                    if track.metadata.get(m) is None:
                        track.metadata[m]=self.metadata.get(m)
    

class Tracks(list):
    """a list of tracks"""
    
    def __init__(self, tracks=[None], mediafile=None):
        """define metadata and file, then extend with Track objects"""
        self.extend(tracks)
        self.mediafile = mediafile
        self.sync()
        
    def __repr__(self):
        """"""
        _str = [f"mediafile: {self.mediafile}"]
        _str.extend(track for track in self)
        return ", ".join((i for i in _str))
    
    def sync(self):
        """update tracks times and positions with parsed values
        
        The applicable time setting is selected based on self list length.
        
        """
        
        def set_starts(times):
            """calculate the starts of tracks from their durations"""
            starts = tuple(s for s in cuetils.timing.starts_from(times))
            for i in range(len(starts)):
                self[i].start = starts[i]
        
        def set_durations(times):
            """calculate the durations of tracks from their starts"""
            durations = tuple(d for d in cuetils.timing.durations_from(times))
            for i in range(len(durations)):
                self[i].duration = durations[i]
        
        def set_positions():
            """set track numbers based on metadata or index"""
            for track in self:
                if not track.number:
                    tnum = track.metadata.get("tracknumber")
                    if tnum:
                        track.number = tnum
                    else:
                        track.number = self.index(track)+1
        
        if len(self) > 0:
            times = tuple(track.time for track in self)
            if self[0].time == 0:
                for track in self: track.start = track.time
                set_durations(times)
            else:
                for track in self: track.duration = track.time
                set_starts(times)
            set_positions()
    
    def ffmpeg(self):
        """chunk a series of tracks to a given base directory
        
        All tracks must have a chunk point (start index) before
        the series can be processed.
        
        TODO: move to ffmpeg module
        
        """
        
        def _cmd(track,mediafile):
            """set an ffmpeg command to be run
            
            The track information will be converted to ffmpeg arguments.
            
            """
            ffcmd = ["ffmpeg","-y","-i",str(mediafile)]
            ffcmd.extend("-c copy -map_metadata -1".split())
            excluded_metadata = ("file")
            ffargs = ["-ss",track.start.format()]
            if track.duration is not None:
                ffargs.extend(["-t",track.duration.format()])
            ffargs.extend( ["-metadata",f"tracknumber={track.number}"])
            for k, v in track.metadata.items():
                if k not in excluded_metadata:
                    ffargs.extend(["-metadata",f"{k}={v}"])
            media_out = mediafile.with_name("{}-{:02d}".format(
                mediafile.stem,track.number)).with_suffix(
                    mediafile.suffix)
            ffargs.append(str(media_out))
            return ffcmd+ffargs
        
        assert not(
            any((t.start is None for t in self))
        ), "all tracks must have a start index."
        for track in self:
            cmd = _cmd(track,self.mediafile)
            print(cmd)
            #subprocess.run(cmd)
    

class Track(object):
    """a generic track"""
    
    def __init__(self, time, metadata={}, number=0, datatype="AUDIO"):
        """define the core properties of a track"""
        self.time = time
        self.start, self.duration = None, None # guessed from time
        self.metadata = Metadata(metadata)
        self.metadata.normalize()
        self.number = number
        self.datatype = datatype
    
    def __repr__(self):
        """"""
        return f"{self.number}: {self.time}"
    

class Metadata(dict):
    """metadata as name:value dictionary"""
    
    def __repr__(self):
        """"""
        return "{"+",".join(
            (f"{repr(k)}:{repr(v)}" for k, v in self.items()
            if not k.startswith("_")))+"}"
    
    def normalize(self):
        """make sure metadata fields are named consistently"""
        for k in tuple(self.keys()):
            self[k.lower()] = self.pop(k)
    


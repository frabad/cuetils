import pathlib
import configparser

class Tags(object):
    """"""

    def __init__(self, cfgfile: pathlib.Path, section: str):
        """import tags a configuration (ini) file"""
        self.tags = {}
        self.config = configparser.ConfigParser(interpolation=None)
        if cfgfile.exists():
            self.config.read(cfgfile)
            if section in self.config:
                self.tags.update(self.config[section])

    def query(self, tag=None):
        """query a tag by its name

        if no specific tag is queried, then all available are shown

        """
        def show(tag):
            value = None
            if tag:
                value = self.tags.get(tag)
                return "%s=%s" % (tag, (repr(value) if value and " " in value
                                        else value))
        return show(tag) if tag else (
            "\n".join([show(i) for i in self.tags]))

    def merge(self, edits={}):
        """merge and cleanup"""
        unwanted = ("""
            handler_name major_brand minor_version vendor_id source_id
            compatible_brands _statistics_writing_date_utc
            _statistics_writing_app _statistics_tags
            """).split()
        for i in self.tags:
            if i in (None,"None",""): unwanted.append(i)
        self.tags.update(edits)
        for i in unwanted:
            if i in self.tags:
                self.tags.pop(i)


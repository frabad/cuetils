"""
Parse and process time cues or durations either as seconds or CDDA frames

"""
import re

def durations_from(times):
    """convert every start time in a list to a duration"""
    for i, time in enumerate(times):
        if i != len(times)-1: # not the last one
            cur = time
            nxt = times[i+1]
            yield time.reset(nxt-cur)

def starts_from(times):
    """convert every duration of a list to a start time"""
    start = 0
    for time in times:
        yield time.reset(start)
        start = start + time

class Time(float):
    """a time value set from a parsed cue

    Keeps the parsed cue as a string property.
    Is converted to a number of seconds or CDDA frames

    """

    def __new__(cls, frags=0, string=""):
        """new immutable instance of float"""
        return float.__new__(cls, frags)

    def __init__(self, frags=0, string=""):
        """set a start time or duration"""
        float.__init__(frags)
        self.string = string

    @classmethod
    def parse(cls, _str):
        """parse a time cue

        Returns:
            either Frames or Seconds

        Important:
            Try Frames first, as the Seconds parser is more lax
            and would work as well. Time offsets in seconds
            would make a non-compliant cuesheet.

        """
        for parse in (Frames.parse, Seconds.parse):
            time = parse(_str)
            if time is not None: # 0 is a valid time value
                return time


class Frames(Time):
    """a number of CDDA frames

    Can be formatted as a time cue, converted to seconds,
    or reset (preserving original string)

    """

    def reset(self, frames):
        """new instance with same string but diffent number of frames"""
        return Frames(frames, self.string)

    def format(self):
        """format a number of frames as mm:ss:ff time cue"""
        s, f = divmod(self, 75)
        m, s = divmod(s, 60)
        #h, m = divmod(m, 60)
        #if h: return "%1d:%02d:%02d:%03d" % (h, m, s, f)
        return "%02d:%02d:%02d" % (m, s, f)

    def to_seconds(self):
        """convert frames to a number of seconds"""
        return Seconds(self / 75)

    def to_frames(self):
        """blind-convert self to frames"""
        return self

    @classmethod
    def parse(cls, _str):
        """parse a time cue in mm:ss:ff format

        mm are minutes, ss are seconds, ff are frames

        """
        pattern = r"\d{2}:\d{2}:\d{2}$"
        p = re.compile(pattern)
        result = p.search(_str)
        if result:
            string = _str[result.start():result.end()]
            msf = string.split(":")
            m, s, f = int(msf[0]), int(msf[1]), int(msf[2])
            return Frames((m*60+s)*75+f, string)

class Seconds(Time):
    """a number of seconds

    Can be formatted as a time cue, converted to frames,
    or reset (preserving original string)

    """

    def reset(self, seconds):
        """new instance with same string but diffent number of seconds"""
        return Seconds(seconds, self.string)

    def format(self):
        """format a number of seconds as a H:MM:SS.sss time cue"""
        m, s = divmod(self, 60)
        h, m = divmod(m, 60)
        return "%1d:%02d:%06.3f" % (h, m, s)

    def to_frames(self):
        """convert a number of seconds to a number of frames"""
        return Frames(self * 75)

    def to_seconds(self):
        """blind-convert self to seconds"""
        return self

    @classmethod
    def parse(cls, _str):
        """parse a time cue in (H:M)M:SS(.sss) format

        H are hours, MM are minutes, SS are seconds, sss are milliseconds

        """
        pattern = r"((\d{1}:)?\d{1,2}:\d{2}(\.\d{3})?)|\d{1}(\.\d{3})?$"
        p = re.compile(pattern)
        result = p.search(_str)
        if result:
            string = _str[result.start():result.end()]
            hms = string.split(":")
            h, m, s = 0, 0, 0.000
            if len(hms) == 3:
                h, m, s = int(hms[0]), int(hms[1]), float(hms[2])
            elif len(hms) == 2:
                m, s = int(hms[0]), float(hms[1])
            elif len(hms) == 1:
                s = float(hms[0])
            return Seconds((h*60+m)*60+s, string)


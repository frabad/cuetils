#!/usr/bin/env python3

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import datetime
import subprocess
import pathlib
import setuptools

shrun = lambda x: subprocess.run(x.split() if isinstance(x,str) else x,
    stdout=subprocess.PIPE,check=True).stdout.decode().strip()

class Info(object):
    
    def _version(self):
        stamp = int(shrun("git log -1 --format=%ct"))
        date = datetime.datetime.utcfromtimestamp(stamp).timetuple()
        return ".".join((str(i) for i in (date.tm_year,date.tm_yday)))
    
    def __init__(self, rc):
        """get info from a resource file"""
        rc = pathlib.Path(pathlib.Path.cwd() / rc)
        with open(rc,"r") as f:
            lines = [line.strip(' #\n') for line in f if line.strip()]
        self.name= lines[0].lower()
        self.description= lines[1]
        self.long_description= "\n".join(lines[2:])
        self.version= self._version()
    

info = Info("README.md")

setuptools.setup(
    name=info.name,
    description=info.description,
    long_description=info.long_description,
    version=info.version,
    url=shrun("git config --get remote.origin.url"),
    author=shrun("git config --get user.name"),
    author_email=shrun("git config --get user.email"),
    platforms=['any'],
    license='GPL-3.0',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Programming Language :: Python :: 3",
    ],
    python_requires=">=3.6",
    packages=setuptools.find_packages(),
    scripts = ["bin/cueproc","bin/cuetag",],
)


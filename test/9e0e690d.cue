REM GENRE Nu-Metal
REM DATE 1997
REM DISCID A20E6A0D
REM COMMENT "ExactAudioCopy v1.0b1"
PERFORMER "Limp Bizkit"
TITLE "Three Dollar Bill, Yall$"
FILE "01 - Intro.wav" WAVE
  TRACK 01 AUDIO
    TITLE "Intro"
    PERFORMER "Limp Bizkit"
    PREGAP 00:00:32
    INDEX 01 00:00:00
FILE "02 - Pollution.wav" WAVE
  TRACK 02 AUDIO
    TITLE "Pollution"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "03 - Counterfeit.wav" WAVE
  TRACK 03 AUDIO
    TITLE "Counterfeit"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "04 - Stuck.wav" WAVE
  TRACK 04 AUDIO
    TITLE "Stuck"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "05 - Nobody Loves Me.wav" WAVE
  TRACK 05 AUDIO
    TITLE "Nobody Loves Me"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "06 - Sour.wav" WAVE
  TRACK 06 AUDIO
    TITLE "Sour"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "07 - Stalemate.wav" WAVE
  TRACK 07 AUDIO
    TITLE "Stalemate"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "08 - Clunk.wav" WAVE
  TRACK 08 AUDIO
    TITLE "Clunk"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "09 - Faith.wav" WAVE
  TRACK 09 AUDIO
    TITLE "Faith"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
  TRACK 10 AUDIO
    TITLE "Stinkfinger"
    PERFORMER "Limp Bizkit"
    INDEX 00 02:26:35
FILE "10 - Stinkfinger.wav" WAVE
    INDEX 01 00:00:00
FILE "11 - Indigo Flow.wav" WAVE
  TRACK 11 AUDIO
    TITLE "Indigo Flow"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "12 - Leech (Demo Version).wav" WAVE
  TRACK 12 AUDIO
    TITLE "Leech (Demo Version)"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00
FILE "13 - Everything.wav" WAVE
  TRACK 13 AUDIO
    TITLE "Everything"
    PERFORMER "Limp Bizkit"
    INDEX 01 00:00:00

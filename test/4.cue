PERFORMER 'Ciolkowska'
TITLE 'Pistolet Budushchego'
REM DATE '2015-10-05'
REM YOUTUBE_ALBUMID 'DcbiYZNu7lo'
REM MUSICBRAINZ_ALBUMID '8687a730-4157-44fd-822e-aab780834eaa'
REM WEBSITE 'https://raig.bandcamp.com/album/ciolkowska-pistolet-budushchego'
REM BANDCAMP_ALBUMID 'a1886336083'
REM ALBUMID 'a1886336083'
FILE 'DcbiYZNu7lo.oga' WAVE
  TRACK 01 AUDIO
    INDEX 01 00:00:00
    TITLE 'Aspera/Astra'
  TRACK 02 AUDIO
    INDEX 01 11:01:00
    TITLE 'Bang-Utot (Koshmar Pirata)'
  TRACK 03 AUDIO
    INDEX 01 14:29:00
    TITLE 'Eject (Jam)'
  TRACK 04 AUDIO
    INDEX 01 20:05:00
    TITLE "Zauryadnaya Radost'"
  TRACK 05 AUDIO
    INDEX 01 28:47:00
    TITLE 'Nizhe Travy (Jam)'
  TRACK 06 AUDIO
    INDEX 01 36:11:00
    TITLE 'Belka i Strelka'
  TRACK 07 AUDIO
    INDEX 01 46:29:00
    TITLE 'Tuda b (2dub)'
  TRACK 08 AUDIO
    INDEX 01 56:17:00
    TITLE 'Tao Te Ching'
